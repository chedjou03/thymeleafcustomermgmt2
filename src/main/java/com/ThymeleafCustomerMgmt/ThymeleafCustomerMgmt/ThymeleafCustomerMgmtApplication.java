package com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafCustomerMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafCustomerMgmtApplication.class, args);
	}

}
