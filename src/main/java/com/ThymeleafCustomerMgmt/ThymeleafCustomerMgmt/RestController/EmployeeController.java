package com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.RestController;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Entity.Employee;
import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Service.EmployeeService;



@Controller
@RequestMapping("/employees")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	//generate a mapping for the "/list"
	@GetMapping("/list")
	public String listEmployee(Model theModel) {
		List<Employee> employees =  employeeService.getEmployees();
		theModel.addAttribute("employees", employees);
		return "employees/list-employee";
	}
	
	@GetMapping("/showAddEmployeeForm")
	public String showEmployeeForm(Model theModel) {
		Employee theEmployee = new Employee();
		List<String> departments = getListDepartment();
		theModel.addAttribute("employee",theEmployee);
		theModel.addAttribute("departments",departments);
		return("employees/employee-form");
	}

	private List<String> getListDepartment() {
		List<String> departments = new ArrayList<String>();
		departments.add("Legal");
		departments.add("Engineering");
		departments.add("HR");
		departments.add("Sales");
		return departments;
	}
	
	@PostMapping("/saveEmployee")
	private String saveEmployee(@ModelAttribute("employee") Employee theEmployee) {
		employeeService.saveEmployee(theEmployee);
		return "redirect:/employees/list";
	}
	
	@GetMapping("/showUpdateEmployeeForm")
	private String showEmployeeFormForUpdate(@RequestParam("employeeId") int theEmployeeId, Model theModel) {
		Employee theEmployee = employeeService.getEmployeeById(theEmployeeId);
		theModel.addAttribute("employee", theEmployee);
		List<String> departments = getListDepartment();
		theModel.addAttribute("departments",departments);
		return("employees/employee-form");
	}
	
	@GetMapping("/deleteEmployee")
	private String deleteEmployee(@RequestParam("employeeId") int theEmployeeId, Model theModel) {
		employeeService.deleteEmployee(theEmployeeId);
		return listEmployee(theModel);
	}

}
