package com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Service;

import java.util.List;
import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Entity.Employee;

public interface EmployeeService {
	
	public List<Employee> getEmployees();

	public void saveEmployee(Employee theEmployee);

	public Employee getEmployeeById(int theEmployeeId);

	public void deleteEmployee(int theEmployeeId);
	
}
