package com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Dao.EmployeeDao;
import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Entity.Employee;

@Service
public class EmployeeServiceManager implements EmployeeService {

	// need to inject Employee dao
	@Autowired
	private  EmployeeDao employeeDao;
		
	@Override
	@Transactional
	public List<Employee> getEmployees() {
		return employeeDao.getEmployees();
	}

	@Override
	@Transactional
	public void saveEmployee(Employee theEmployee) {
		employeeDao.saveEmployee(theEmployee);
	}

	@Override
	@Transactional
	public Employee getEmployeeById(int theEmployeeId) {
		return employeeDao.getEmployeeById(theEmployeeId);	
	}

	@Override
	@Transactional
	public void deleteEmployee(int theEmployeeId) {
		employeeDao.deleteEmployee(theEmployeeId);		
	}

}
