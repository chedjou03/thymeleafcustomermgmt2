package com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ThymeleafCustomerMgmt.ThymeleafCustomerMgmt.Entity.Employee;

@Repository
public class EmployeeDaoManager implements EmployeeDao{

	
	
	
	//define field for entity manager
	private EntityManager entityManager;
		
	//set up constructor injection
	@Autowired
	public EmployeeDaoManager(EntityManager theEntityManager)
	{
		entityManager = theEntityManager;
	}
			
	@Override
	public List<Employee> getEmployees() {
		
		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		// create a query  ... sort by last name
		Query<Employee> theQuery = currentSession.createQuery("from Employee order by lastName",Employee.class);
		
		// execute query and get result list
		List<Employee> employees = theQuery.getResultList();
				
		// return the results		
		return employees;
	}

	@Override
	public void saveEmployee(Employee theEmployee) {
		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.saveOrUpdate(theEmployee);
		
	}

	@Override
	public Employee getEmployeeById(int theEmployeeId) {
		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		Employee theEmployee = currentSession.get(Employee.class,theEmployeeId);
		return theEmployee;
		
	}

	@Override
	public void deleteEmployee(int theEmployeeId) {
		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		Query theQuery = currentSession.createQuery("delete from Employee where id =:employeeId");
				theQuery.setParameter("employeeId", theEmployeeId);
				theQuery.executeUpdate();	
	}
}
